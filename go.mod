module git.sr.ht/~rumpelsepp/tun

go 1.13

require (
	git.sr.ht/~rumpelsepp/rlog v0.0.0-20190826091145-a50111d2adbe
	golang.org/x/sys v0.0.0-20191024073052-e66fe6eb8e0c
)
